FROM alpine:3.8
LABEL maintainer "Gordon Lenz <golenz@mailbox.org>"

RUN apk update && apk add --no-cache \
        --repository http://dl-3.alpinelinux.org/alpine/edge/testing/ \
        alpine-sdk \
        bison \
        flex \
        gcc \
        dtc \
        gcc-arm-none-eabi \
        git \
        make \
        python2 \
        python2-dev \
        swig
